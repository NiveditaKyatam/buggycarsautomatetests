package Objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegisterPage extends PageObject
{

   private final String LOGIN = "TESTNIVE8";
   private final String FNAME = "TEST";
   private final String LNAME = "NIVE";
   private final String PASSWORD = "Welcome@01";


   @FindBy(id = "username")
   private WebElement username;

   @FindBy(id = "firstName")
   private WebElement firstName;

   @FindBy(id = "lastName")
   private WebElement lastName;

   @FindBy(css = "#password")
   private WebElement mainPassword;

   @FindBy(id = "confirmPassword")
   private WebElement confirmPassword;

   @FindBy(xpath = "//button[contains(text(),'Register')]")
   private WebElement btnRegister;

   @FindBy(xpath = "//button[contains(text(),'Cancel')]")
   private WebElement btnCancel;

   @FindBy(xpath = "//div[contains(text(),'UsernameExistsException: User already exists')]")
   private WebElement registerFailure;

   @FindBy(xpath = "//div[contains(text(),'Registration is successful')]")
   private WebElement registerSuccess;

   public RegisterPage(WebDriver driver) {
        super(driver);
    }

       public void enterRegisterdetails()
       {

           this.username.sendKeys(LOGIN);
           this.firstName.sendKeys(FNAME);
           this.lastName.sendKeys(LNAME);
           this.mainPassword.sendKeys(PASSWORD);
           this.confirmPassword.sendKeys(PASSWORD);
           this.btnRegister.click();
       }

       public void enterRegisterdetailsinvalid()
       {

           this.enterRegisterdetails();
       }

       public void verifyRegisterFailure()
       {
           this.registerFailure.isDisplayed();
       }
       public void verifyRegisterSuccess()
       {
           this.registerSuccess.isDisplayed();
       }

   }



