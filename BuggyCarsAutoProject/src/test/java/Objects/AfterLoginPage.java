package Objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class AfterLoginPage extends PageObject {


    @FindBy(css = "li.nav-item:nth-child(2) > a:nth-child(1)")
    private WebElement logoutLink;

    @FindBy(css = "li.nav-item:nth-child(3) > a:nth-child(1)")
    private WebElement profileLink;

    @FindBy(xpath = "/html/body/my-app/header/nav/div/my-login/div/form/div/span")
    private WebElement alertFailure;

    public AfterLoginPage(WebDriver driver) {
        super(driver);
    }

    public void verifyLoginFailure() {
        this.alertFailure.isDisplayed();
    }
    public void verifyLoginSuccess() {
    this.profileLink.isDisplayed();
    }

    public void clickLogout() {
        this.logoutLink.click();
    }
}