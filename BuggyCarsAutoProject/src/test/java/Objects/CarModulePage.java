package Objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.concurrent.TimeUnit;

public class CarModulePage extends PageObject {

    private final String VOTE_COMMENT = "Nivedita";

    @FindBy(xpath = "//img[@title='Diablo']")
    private WebElement imagecar;

    @FindBy(id = "comment")
    private WebElement votecomment;

    @FindBy(xpath = "/html/body/my-app/div/main/my-model/div/div[1]/div[3]/div[2]/div[2]/div/button")
    private WebElement vote_button;


    public CarModulePage(WebDriver driver)
    {
        super(driver);
    }

    public void enterVote(){
        this.imagecar.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        this.votecomment.click();
        this.votecomment.sendKeys(VOTE_COMMENT);
        this.vote_button.click();
    }

}