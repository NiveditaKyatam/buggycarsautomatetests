package Objects;

import org.openqa.selenium.WebDriver;
        import org.openqa.selenium.WebElement;
        import org.openqa.selenium.support.FindBy;

public class LoginPagee extends PageObject {

    private final String CUSER_NAME = "Nivedita6";
    private final String CPASSWORD = "Welcome@01";
    private final String IUSER_NAME = "USER_NAME";
    private final String IPASSWORD = "PASSWORD";

    @FindBy(name = "login")
    private WebElement login;

    @FindBy(name = "password")
    private WebElement password;

    @FindBy(xpath = "//button[contains(text(),'Login')]")
    private WebElement Login_button;


    public LoginPagee(WebDriver driver) {
        super(driver);
    }

    public void entervalidUserName() {
        this.login.sendKeys(CUSER_NAME);
    }

    public void entervalidPassword() {
        this.password.sendKeys(CPASSWORD);
    }

    public void enterinvalidUserName() {
        this.login.sendKeys(IUSER_NAME);
    }

    public void enterinvalidPassword() {
        this.password.sendKeys(IPASSWORD);
    }

    public void clickLoginButton() {
        this.Login_button.click();
    }
}
