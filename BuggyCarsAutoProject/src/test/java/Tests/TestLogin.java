package Tests;

import Objects.AfterLoginPage;
import Objects.LoginPagee;
import Utils.Utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class TestLogin {
    private static final WebDriver driver = new ChromeDriver();
    private static LoginPagee loginPage = new LoginPagee(driver);
    private static AfterLoginPage afterLoginPage = new AfterLoginPage(driver);

    @BeforeSuite
    public void startup() {
        // ChromeDriver location set up in Utils.Utils class
        System.setProperty("webdriver.chrome.driver", Utils.CHROME_DRIVER_LOCATION);
    }


    @Test(testName = "Login to the Application - Success")
    public static void successLogin(){

        driver.get(Utils.BASE_URL);
        loginPage.entervalidUserName();
        loginPage.entervalidPassword();
        loginPage.clickLoginButton();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        afterLoginPage.verifyLoginSuccess();
        afterLoginPage.clickLogout();

    }


    @Test(testName = "Login to the Application - Failure")
    public static void failLogin()
    {

        driver.get(Utils.BASE_URL);
        loginPage.enterinvalidUserName();
        loginPage.enterinvalidPassword();
        loginPage.clickLoginButton();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        afterLoginPage.verifyLoginFailure();

    }

    @AfterSuite
    public static void cleanUp()
    {
        driver.manage().deleteAllCookies();
        driver.close();
    }
}