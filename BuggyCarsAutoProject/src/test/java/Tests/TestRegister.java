package Tests;

import Objects.RegisterPage;
import Utils.Utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;


public class TestRegister
{


    private static final WebDriver driver = new ChromeDriver();
    private static RegisterPage registerPage = new RegisterPage(driver);

    @BeforeSuite
    public void startup()
    {
        // ChromeDriver location set up in Utils.Utils class
        System.setProperty("webdriver.chrome.driver", Utils.CHROME_DRIVER_LOCATION);
    }

    @Test(testName = "Register to the Application - Success")
    public static void submitSuccessRegister()
    {

        driver.get(Utils.REG_URL);
        registerPage.enterRegisterdetails();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        registerPage.verifyRegisterSuccess();


    }

    @Test(testName = "Register to the Application - Failure")
    public static void submitFailureRegister()
    {

        driver.get(Utils.REG_URL);
        registerPage.enterRegisterdetails();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        registerPage.verifyRegisterFailure();


    }


    @AfterSuite
    public static void cleanUp()
    {
        driver.manage().deleteAllCookies();
        driver.close();
    }
}
